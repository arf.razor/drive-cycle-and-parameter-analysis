from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
import pandas as pd 
import numpy as np
import os 

# Defining global variables: 
# Valid signals for exceptions
EXCEPTIONAL_SIGNALS = ['altitude', 'time', 'speed', 
                    'cal_distance_covered']

# Valid signals for most datasets
VALID_SIGNALS = ['altitude_gps', 'time', 'speed', 
                    'cal_distance_covered']

# Constants
GRAVITATIONAL_CONSTANT = 9.81 # [m/s^2]
LOWER_BOUND_SPEED = 0.3 # [m/s]
LOWER_BOUND_ACC = 0.1 # [m/s]
AIR_DENSITY = 1.293 # [kg/m^3
SAMPLE_RATE = 1000 # Signals are probably recorder with 1000 Hz]
MPS_TO_KPH = 3.6  # Factor to calulate kph from mps 
WS_TO_KWH = 1/(3600 * 1000) # Factor to convert Ws into kWh
 


# class definitions
class DriveCycle : 
    '''
    This class represents a singular drive cycle from the dataset.

    Args:
        filepath (str): The path to the file containing a singular drive cycle data.

    Attributes:
        dataframe (pandas.DataFrame): The processed drive cycle listed as a time dependent DataFrame.
        cycle_id (str): The identifier of the drive cycle.
        total_distance (float): The total distance covered during the drive cycle in meters.
        total_mean_speed (float): The mean speed of the drive cycle in kilometers per hour.
        nonzero_mean_speed (float): The mean speed of the drive cycle, excluding zero speed values, in kilometers per hour.
        climbed_altitude (float): The total altitude climbed during the drive cycle.
    '''

    def __init__(self, filepath): 

        '''
        Initializes a DriveCycle object with the data from the given file.

        Args:
            filepath (str): The path to the file containing the drive cycle data.

        '''

        try: 
            # Extract the drive cycles most important signals
            self.dataframe = pd.read_csv(filepath, usecols = VALID_SIGNALS)
            # rename dataframe columns to unify 
            self.dataframe = self.dataframe.rename(columns = {'altitude_gps' : 'Altitude',
                                                               'cal_distance_covered' : 'Distance',
                                                                'speed' : 'Speed', 'time' : 'Time'})
            self.dataframe = self.dataframe.reindex(sorted(self.dataframe.columns), axis=1)  
        except: 
            # One use-case has a different naming convention for their 
            self.dataframe = pd.read_csv(filepath, usecols = EXCEPTIONAL_SIGNALS)
            # rename dataframe columns to unify 
            self.dataframe = self.dataframe.rename(columns = {'altitude': 'Altitude',
                                                               'cal_distance_covered' : 'Distance',
                                                                'speed' : 'Speed', 'time' : 'Time'})
            self.dataframe = self.dataframe.reindex(sorted(self.dataframe.columns), axis=1)  
        
        # Get identifier for every instance of 'Cycle' from filepath
        self.cycle_id = (os.path.basename(filepath).split('/')[-1]).split('.')[0]

        # Calculate Distance to be in meters
        self.dataframe['Distance'] = self.dataframe['Distance'] * 1000  

        # Set total driven distance of drive cycle
        self.total_distance = self.dataframe['Distance'].max() # Distance in meters

        # Get total mean velocity 
        self.total_mean_speed = MPS_TO_KPH * (self.dataframe['Speed'].mean()) # [kph]
        
        # Get nonzero mean velocity
        self.nonzero_mean_speed_df = self.dataframe['Speed'].loc[self.dataframe['Speed'] > LOWER_BOUND_SPEED]
        self.nonzero_mean_speed = self.nonzero_mean_speed_df.mean() * MPS_TO_KPH # [kph]
        # self.nonzero_mean_speed = self.dataframe['Speed'].fillna(0)

        # Set all velocities below threshhold to zero
        self.dataframe.loc[self.dataframe['Speed'] < LOWER_BOUND_SPEED, 'Speed'] = 0
        
        # Turn dataframe into time dependent values, interpolate time values to be in seconds
        self.dataframe['Time'] = pd.to_timedelta(((self.dataframe['Time'] - self.dataframe['Time'].loc[0]))
                                                 , unit='ms')
        self.dataframe = self.dataframe.resample('S', on = 'Time').mean()
        self.dataframe.index = self.dataframe.index.seconds # turns dates back into integer values        

        # Get Array of gradients and altitude climbed in total
        self.dataframe['Gradient'] = np.arctan(self.dataframe['Altitude'].diff() 
                                               / self.dataframe['Distance'].diff())
        self.dataframe['Gradient'] = self.dataframe['Gradient'].fillna(0) 

        # Get total climbed altitude (Sum all altitude differences that are greater than zero)
        self.climbed_altitude = (self.dataframe['Altitude'].diff()).where(self.dataframe['Altitude'].diff() > 0).sum() 



class VehicleParameters: 
    '''
    This class represents the parameters of a vehicle.

    If no input values are provided, the class will initialize itself to represent a Volkswagen ID3 Pro Performance.

    Args:
        air_drag_coeff (float): The unitless air drag coefficient of the vehicle (default: 0.267).
        dynamic_tire_radius (float): The vehicle's dynamic tire in meters (default: 0.3468).
        frontal_surface_area (float): The vehicle's frontal surface area in square meters (default: 2.8365).
        vehicle_mass (float): The mass of the vehicle in kilograms (default: 1811).
        vehicle_id (str): The identifier of the vehicle (default: 'VW_ID3').

    Attributes:
        vehicle_id (str): The identifier of the vehicle.
        air_drag_coeff (float): The air drag coefficient of the vehicle.
        dynamic_tire_radius (float): The dynamic tire radius of the vehicle.
        frontal_surface_area (float): The frontal surface area of the vehicle.
        rolling_resistance_coeff (float): The rolling resistance coefficient of the vehicle.
        mass (int): The total mass of the vehicle, including the assumed driver weight (default: vehicle_mass + 80).
    '''

    # If no input values are presented, the class will initiate itself to a Volkswagen ID3 Pro Performance
    def __init__(self, air_drag_coeff = 0.267, dynamic_tire_radius = 0.3468,
                 frontal_surface_area = 2.8365, vehicle_mass = 1811, vehicle_id = 'VW_ID3'):  
        '''
        Initializes a VehicleParameters object with the given parameters.

        Args:
            air_drag_coeff (float): The coefficient of air drag for the vehicle (unitless).
            dynamic_tire_radius (float): The dynamic tire radius of the vehicle in meters.
            frontal_surface_area (float): The frontal surface area of the vehicle in square meters.
            vehicle_mass (float): The mass of the vehicle without the driver in kilograms.
            vehicle_id (str): The identifier for the vehicle.

        '''

        self.vehicle_id = vehicle_id
        self.air_drag_coeff = air_drag_coeff # unitless
        self.dynamic_tire_radius = dynamic_tire_radius # [m]
        self.frontal_surface_area = frontal_surface_area # [m^2]
        self.rolling_resistance_coeff = 0.008 # for now assumed to be a fix, unitless value 
        self.mass = vehicle_mass + 80 # [kg] assume driver to weigh 80kg 



class Route: 
    '''
    Combines a track object and a vehicle parameter object into a route.

    Attributes:
        vehicle (Vehicle): A vehicle object that is defined outside and given as a constructor parameter.
        cycle (Cycle): A vehicle object that is defined outside and given as a constructor parameter.
        id (str): The identifier for the route, a combination of cycle ID and vehicle ID.
        dataframe (DataFrame): A copy of the cycle's dataframe.
        positive_power (Series): Series of power values greater or equal to zero, calculated from resistance forces.
        positive_energy (float): Total positive energy at the wheels, calculated as the integral of positive power.
        negative_power (Series): Series of power values smaller than zero, calculated from resistance forces.
        negative_energy (float): Total negative energy at the wheels (recuperation energy), calculated as the integral of negative power.
        resulting_energy (float): Resulting energy consumption, obtained by adding positive and negative energies.

    Methods:
        __init__(self, cycle, vehicle): Initializes a Route object.

    '''

    def __init__(self, cycle, vehicle): 
        '''
        Initializes a Route object with a given cycle and vehicle object.

        Args:
            cycle (Cycle): The cycle object associated with the route.
            vehicle (Vehicle): The vehicle object associated with the route.

        '''

        # Get vehicles object data
        self.vehicle = vehicle
        self.cycle = cycle

        # Set identificators for Route
        self.id = self.cycle.cycle_id + '_' + self.vehicle.vehicle_id 
        
        # Copy dataframe
        self.dataframe = cycle.dataframe.copy(deep=True)
        
        # Calculate Acceleration 
        self.dataframe['Acceleration'] = self.dataframe['Speed'].diff()
        
        # Initiate power vector 
        self.dataframe['Power'] = np.zeros(len(self.dataframe))
        
        # Initiate resistance forces 
        self.dataframe['Acceleration Resistance'] = np.zeros(len(self.dataframe))
        self.dataframe['Rolling Resistance'] = np.zeros(len(self.dataframe))
        self.dataframe['Slope Resistance'] = np.zeros(len(self.dataframe))
        self.dataframe['Air Drag'] = np.zeros(len(self.dataframe))

        for ts in range(len(cycle.dataframe)): 
            if self.dataframe.loc[ts, 'Speed'] > LOWER_BOUND_SPEED: 
                # Calculate Rolling resistance
                self.dataframe.loc[ts, 'Rolling Resistance'] = (vehicle.rolling_resistance_coeff * vehicle.mass
                                                                * np.cos(self.dataframe.loc[ts, 'Gradient'])
                                                                * GRAVITATIONAL_CONSTANT)
                # Calculate Air Drag
                self.dataframe.loc[ts, 'Air Drag'] = (vehicle.air_drag_coeff * vehicle.frontal_surface_area 
                                                      * AIR_DENSITY/2 * (self.dataframe.loc[ts, 'Speed'])**2)
                # Calculate Slope Resistance 
                self.dataframe.loc[ts, 'Slope Resistance'] = (vehicle.mass * GRAVITATIONAL_CONSTANT 
                                                              * np.sin(self.dataframe.loc[ts, 'Gradient']))
                # Calculate Acceleration Resistance
                self.dataframe.loc[ts, 'Acceleration Resistance'] = (self.dataframe.loc[ts, 'Acceleration']
                                                                     * vehicle.mass)
                # Calculate Resulting power for every timestep 
                self.dataframe.loc[ts, 'Power'] = ((self.dataframe.loc[ts, 'Acceleration Resistance']
                                                   + self.dataframe.loc[ts, 'Rolling Resistance'] 
                                                   + self.dataframe.loc[ts, 'Slope Resistance']
                                                   + self.dataframe.loc[ts, 'Air Drag'])
                                                   * self.dataframe.loc[ts, 'Speed'])

        # Positive energy at the wheels
        self.positive_power = self.dataframe.loc[self.dataframe['Power'] >= 0, 'Power']
        self.positive_energy = np.trapz(self.positive_power)
        self.positive_energy = self.positive_energy * WS_TO_KWH

        # Negative energy at the wheels (Recuperation Energy)
        self.negative_power = self.dataframe.loc[self.dataframe['Power'] < 0, 'Power']
        self.negative_energy = np.trapz(self.negative_power)
        self.negative_energy = self.negative_energy * WS_TO_KWH

        # Resulting energy consumption
        self.resulting_energy = self.positive_energy + self.negative_energy

class LinearRegressionObject: 
    '''
    This class represents a collection of linear regression data, used in the parameter sensitivity analysis.

    Attributes:
        x_data (ndarray): The input data for the x-axis.
        y_data (ndarray): The input data for the y-axis.
        x_data_std (ndarray): The standardized input data for the x-axis.
        regr_mdl (LinearRegression): The non-standardized linear regression model.
        regr_mdl_std (LinearRegression): The standardized linear regression model.
        std_coeff (float): The standardized coefficient of the linear regression model.
        y_predict (ndarray): The predicted values from the linear regression model.

    Methods:
        __init__(self, x_data, y_data): Initializes a LinearRegressionObject.

    '''

    def __init__(self, x_data, y_data):   
        '''
        Initializes a LinearRegressionObject with the given input data.

        Args:
            x_data (ndarray): The input data for the x-axis.
            y_data (ndarray): The input data for the y-axis.

        Raises:
            TypeError: If x_data or y_data are not numpy arrays.

        '''

        x_data = x_data.reshape((-1,1))
        self.x_data = x_data
        self.y_data = y_data

        # Standardize x_data
        scaler = StandardScaler()
        self.x_data_std = scaler.fit_transform(x_data)

        # Calculate non standardized regression model
        self.regr_mdl = LinearRegression()
        self.regr_mdl.fit(self.x_data, self.y_data)

        # Calculate standardized regression model
        self.regr_mdl_std = LinearRegression()
        self.regr_mdl_std.fit(self.x_data_std, self.y_data)
        self.std_coeff = self.regr_mdl_std.coef_ / np.std(self.x_data_std, axis=0)
        self.std_coeff = self.std_coeff[0]

        # Non standardized predicition 
        self.y_predict = self.regr_mdl.predict(self.x_data)

