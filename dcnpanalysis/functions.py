from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt 
import plotly.graph_objects as go
from plotly.offline import plot
import pandas as pd
import numpy as np

try:
    import classes
except ImportError:
    from . import classes

import math
import glob
import os

def check_input_data(filepath): 
        '''
        Check if the input folder contains the expected track data.

        Args:
                filepath (str): Path to the input folder.

        Returns:
                bool: True if all expected files are present, False otherwise.

        Raises:
                FileNotFoundError: If the provided folder path does not exist.
        '''

        # Expected filenames
        expected_files = set(f'track_{i}.csv' for i in range(148))

        # Check if the folder exists
        if os.path.exists(filepath):
                # List the contents of the folder
                folder_contents = os.listdir(filepath)

                # Convert the filenames to a set for easy comparison
                actual_files = set(folder_contents)

                # Check if expected filed are present
                if expected_files.issubset(actual_files):
                        return True
                else:
                        # Find missing files
                        missing_files = expected_files - actual_files
                        print("Missing files:")
                        for file in missing_files:
                                print(file)
                        return False
        else:
                raise FileNotFoundError('Please provide a valid folder path')

def find_parameter_cycles(cycle_df, keyword, deviation): 
        '''
        Find drive cycles that keep the two not specified parameters from the list ['Speed', 'Distance', 'Altitude'] constant
        while maximizing the tracks with varying values for the parameter specified by 'keyword'.

        Args:
                cycle_list (pandas.DataFrame): A DataFrame specifying all tracks and the values for three parameters.
                keyword (str): The parameter from the list ['Speed', 'Distance', 'Altitude'] to vary 
                                while keeping the other two parameters constant.
                deviation (float): The allowed deviation percentage for the constant parameters.

        Returns:
            tuple: A tuple containing the following elements:
                - pandas.DataFrame: The reduced input DataFrame containing tracks that vary the specified parameter
                        while keeping the other parameters constant within the allowed deviation.
                - list: list of all track identifiers that remain in the output dataframe 

        Raises:
                ValueError: If the provided DataFrame is empty. 
                TypeError: If the deviation argument is not a float or cannot be converted to a float.
                ValueError: If the deviation is not between 0 and 100.
                TypeError: If the keyword argument is not a string.
                ValueError: If the keyword is not one of the valid options ['Speed', 'Distance', 'Altitude'].
        '''

        # Checks for function attributes
        if cycle_df.empty:
                raise ValueError('The provided DataFrame is empty.')
        if not (0 <= deviation <= 100):
                raise ValueError('The deviation should be entered as a percentage value.'
                                 'Make sure deviation is between 0 and 100.')
        if not isinstance(keyword, str):
                raise TypeError('Please enter the varying parameter as a string.')
        
        valid_keywords = ['Speed', 'Distance', 'Altitude']
        if keyword not in valid_keywords:
                raise ValueError('Please enter one of the following parameters:'
                                ' "Altitude", "Distance" or "Speed"')
        
        # Valid columns
        valid_columns = ['Total Distance [m]', 'Climbed Altitude [hm]', 'Total Mean Speed [km/h]']
        
        # Remove varying parameter from list
        parameter_list = [ x for x in valid_columns if keyword not in x ]

        # Initiate empty dataframe for filtering with respect to first parameter
        first_parameter_filter = pd.DataFrame()
        
        # Iterate over both remaining parameters in parameter_list
        for string in parameter_list:
                # For every value in a column check how many other values within
                # the range allowed by 'deviation' exist. 
                for value in cycle_df[string]:
                        # Set boundaries
                        lower_bound = value * (1 - deviation/100)
                        upper_bound = value * (1 + deviation/100)
                        # Get number of elements in every iteration 
                        current_indices = cycle_df[string].between(lower_bound, upper_bound,
                                                                        inclusive='both')
                        current_df = cycle_df.loc[current_indices]
                        # If the list is longer than the current list 'first_parameter_filter', 
                        # replace the current longest list
                        if len(current_df) > len(first_parameter_filter): 
                                first_parameter_filter = current_df 

                                prm_biggest_list = string
        
        # initiate empty dataframe to be filtered for both parameters
        fully_filtered_df = pd.DataFrame()
        # Reduce parameter list to the last remaining not sorted parameter
        parameter_list = [ x for x in parameter_list if prm_biggest_list not in x ]
        # Do the same process as before, now with the already filtered list. 
        # Only one parameter is left in 'parameter_list'
        for string in parameter_list: 
                # Again for every value check how many values are within 
                # 'deviation' range
                for value in first_parameter_filter[string]: 
                        # Set boundaries 
                        lower_bound = value * (1 - deviation/100)
                        upper_bound = value * (1 + deviation/100)
                        # Get number of elements in every iteration 
                        current_indices = first_parameter_filter[string].between(lower_bound, 
                                                                                upper_bound, 
                                                                                inclusive='both')
                        current_df = first_parameter_filter.loc[current_indices]
                        # If the current dataframe is longer than 'fully_filtered_df'
                        # replace 'fully_filtered_df'
                        if len(current_df) > len(fully_filtered_df): 
                                fully_filtered_df = current_df 
        # return selected cycles
        return fully_filtered_df, list(fully_filtered_df['ID'])

def calculate_measured_energy(validatable_cycle): 
        '''
        Calculate the energy consumption based on mechanical signals of a drive cycle.

        Args:
            validatable_cycle (pandas.DataFrame): A pandas DataFrame representing the drive cycle data.
                Each DataFrame should contain the signals 'engine_rpm' and 'axle_torque_nominal'.

        Returns:
            tuple: A tuple containing the following elements:
                - total_measured_energy (float): The total consumed energy of the cycle.
                - recuperation_energy (float): The energy recovered during regenerative braking.

        Raises:
                ValueError: If the provided drive cycle does not contain the required signals.
        '''

        # 
        if (not any(validatable_cycle.columns.str.contains('engine_rpm')) 
            or not any(validatable_cycle.columns.str.contains('axle_torque_nominal'))):
                raise ValueError('"engine_rpm", "axle_torque_nominal" or both are not present in the column names.')

        # Copy the dataframe
        current_df = validatable_cycle
        current_df = current_df.fillna(value = 0)
        # Add Mechanical Power Vector
        current_df['Mechanical Power'] = np.zeros(len(current_df))
        # Calculate actual mechanical power
        for timestep in range(len(current_df)): 
                # Only use timesteps for calculation where the vehicle actually moves
                if current_df.loc[timestep, 'speed'] > classes.LOWER_BOUND_SPEED:
                        try: 
                                current_df.loc[timestep, 'Mechanical Power'] = (current_df.loc[timestep, 'engine_rpm'] 
                                                                                * current_df.loc[timestep, 'axle_torque_nominal']
                                                                                * (2 * np.pi)/60)
                        except ValueError:
                                print('The provided dataframe does is missing one or more required signas.'
                                      'Make sure the dataframe has an "axle_torque_nominal", "engine_rpm" and a "speed" column.') 
                else: 
                        current_df.loc[timestep, 'Mechanical Power'] = 0
        # Calculate resulting energy by integrating power vector
        total_measured_energy = np.trapz(y = current_df['Mechanical Power'], dx = 0.1)
        recuperation_energy = np.trapz(y = current_df['Mechanical Power'][current_df['Mechanical Power'] < 0], dx = 0.1)
        # Convert to kWh
        total_measured_energy = total_measured_energy * classes.WS_TO_KWH
        recuperation_energy = recuperation_energy * classes.WS_TO_KWH
        # 
        return total_measured_energy, recuperation_energy

def list_cycles(data_path):
        '''
        List and load tracks all tracks that are saved as CSV files from the specified directory.

        Args:
                data_path (str): The path to the directory containing the CSV files.

        Returns:
            tuple: A tuple containing the following elements:
                - pandas.Dataframe: Dataframe that contains the loaded tracks, where each row
                                 represents a track and each column a respective track parameter
                - dictionary: Dictionary contains the loaded tracks as track objects, with their respective IDs as keys.

        Raises:
                ValueError: If the specified data path is not a valid directory.
        '''

        # Check function inputs
        if check_input_data(data_path) == False:
                raise ValueError('Please provide the full dataset containing all the tracks.')
        # 
        cycle_dict = {}
        # 
        file_list = glob.glob(data_path + "/*.csv")
        cycle_df = pd.DataFrame(columns = ['ID', 'Total Distance [m]', 'Total Mean Speed [km/h]', 
                                             'Climbed Altitude [hm]'])
        for cycle in file_list: 
                # Create a 'cycle' object for each cycle 
                current_cycle = classes.DriveCycle(cycle)
                # For every cycle create a dictionary element containing said cycle
                cycle_dict[current_cycle.cycle_id] = current_cycle
                # Save the notable data in a dataframe
                current_row = pd.DataFrame([{'ID' : current_cycle.cycle_id, 
                                             'Total Distance [m]' : current_cycle.total_distance, 
                                             'Total Mean Speed [km/h]' : current_cycle.total_mean_speed, 
                                             'Climbed Altitude [hm]' : current_cycle.climbed_altitude}])
                # Append notable data to existing dataframe
                cycle_df = pd.concat([cycle_df, current_row])
        # Return list of cycles
        return cycle_df, cycle_dict

def list_validation_cycles(data_path): 
        '''
        List and load cycles from CSV files that contain the signals 'engine_rpm', 'speed',
        'axle_torque_nominal' from the specified directory.

        Args:
                data_path (str): The path to the directory containing the CSV files.

        Returns:
                dict: A dictionary containing the loaded tracks as DataFrames, where the keys are the track IDs.

        Raises:
                ValueError: If the specified data path is not a valid directory.
        '''

        # Check function inputs
        if not os.path.isdir(data_path):
                raise ValueError('The specified data path is not a valid directory.')
        
        files = glob.glob(data_path + "/*.csv")
        validation_tracks = {}
        for cycle in files: 
                try: 
                        # Get Track ID from the .csv file link
                        cycle_id = (os.path.basename(cycle).split('/')[-1]).split('.')[0]
                        current_track = pd.read_csv(cycle, usecols = {'engine_rpm', 'speed',
                                                                                'axle_torque_nominal'})
                        # If nan values are less than 10% the track can be used for validation
                        if (current_track['engine_rpm'].isna().sum() < len(current_track)*0.1 or 
                            current_track['axle_torque'].isna().sum() < len(current_track)*0.1): 
                                validation_tracks[cycle_id] = current_track
                except: 
                        None
        # Return all tracks found to contain the desired signals as a dictionary 
        return validation_tracks

def intersect_cycles(validation_tracks, parameter_tracks): 
        '''
        Find track names present in both data structures. Only valid for modeling of ID3.

        Args:
                validation_tracks (dict): Dictionary containing tracks that can be used for validation. 
                                          Usually output of 'list_validation_tracks()'.
                parameter_tracks (pandas.DataFrame): A DataFrame containing tracks in 'ID' column.
                                                     Usually output of 'find_parameter_tracks()'.

        Returns:
                list: List of track IDs that are present in both data structures.

        Raises:
                TypeError: If the validation_tracks parameter is not of type dict.
                TypeError: If the parameter_tracks parameter is not of type pandas.DataFrame.
        '''

        # Check inputs 
        if not isinstance(validation_tracks, dict):
                raise TypeError('The validation_tracks parameter must be entered as a dictionary.')
        if not isinstance(parameter_tracks, list):
                raise TypeError('The parameter_tracks parameter must be entered as a list.')
        # Actual code
        track_intersect = list(set(validation_tracks.keys()).intersection(parameter_tracks))
        # Output
        return track_intersect

def evaluate_routes(cycles, cycle_dict, vehicle_model):
        '''
        Evaluates a vehicle for every element of the specified tracklist

        Args:
                cycles (list): A list containing any number of tracks that are being applied to a specific vehicle
                cycle_dict (dict): A dictionary containing all available cycles
                vehicle_model (classes.VehicleParameter): A VehicleParameter object containing the specifications
                                                          of the vehicle being evaluated.

        Returns:
                dict: dictionary of Route objects, containing the evaluation of the vehicle on the specified tracks.

        Raises:
                TypeError: If tracks is not of type list.
                TypeError: If the parameter_tracks is not a VehicleParameter object.
        '''

        # Raise errors
        if not isinstance(cycles, list):
                raise TypeError('The tracks parameter has to be entered as a list.')
        if not isinstance(vehicle_model, classes.VehicleParameters):
                raise TypeError('The vehicle_model parameter has to be entered as a VehicleParameter object.')
        # Create empty dictionary to save routes by keys
        route_dict = {}
        # Iterate over listed tracks
        for element in cycles: 
                # for each track: create a route containing the current track and the entered vehicle
                current_route = classes.Route(cycle = cycle_dict[element], vehicle = vehicle_model)
                route_dict[current_route.id] = current_route
        # Return the resulting dictionary
        return route_dict

def validation_plots(cycle_dict, vehicle_parameters, cycles_for_validation, viable_track_list, varying_parameter, saveplot): 
        '''
        Plots the energy consumption calculated from the driving resistance 
        versus the energy consumption from CAN data

        Args:
                route_dict (dict): A dictionary containing all route data for a specified vehicle evaluated
                                   on specified tracks
                cycles_for_validation (dict): A dictionary containing all cycles with the necessary
                                              signals to calculate the energy consumption form CAN Data
                varying_parameter (str): The varied parameter for which the validation cycles were chosen
                saveplot (bool): If true, the plot will be saved but not shown, if false, the other way around.

        Returns:
                -

        Raises:
                TypeError: If cycle_dict is not of type dictionary.
                TypeError: If the cycles_for_validation is not of type dictionary.
                TypeError: If the varying_parameter is not of type string
                TypeError: If the viable_track_list is not of type list
                TypeError: If the saveplot is not of type bool
        '''
        # Raising errors for parameters
        if not isinstance(cycle_dict, dict):
                raise TypeError('Cycle data must be passed as a dictionary.')
        if not isinstance(cycles_for_validation, dict): 
                raise TypeError('Validation cycles must be given as a dictionary.')
        if not isinstance(varying_parameter, str): 
                raise TypeError('Varied parameter must be given as a string.')
        if not isinstance(viable_track_list, list): 
                raise TypeError('List of viable tracks has to be entered as a list')
        if not isinstance(saveplot, bool): 
                raise TypeError('Setting for saving of the plot must be given as a bool.')
        
        # Create list of cycles for validation
        validation_cycle_dict = {
                key: cycles_for_validation[key] 
                for key in viable_track_list
                if key in cycles_for_validation
        }

        # List all tracks that have to be evaluated 
        track_ids = list(validation_cycle_dict.keys())
        # display(track_ids)
       
        # Create empty dictionary
        route_dict = {}
        # Create a route dictionary with all validation tracks
        for track in track_ids: 
                current_route = classes.Route(vehicle = vehicle_parameters, 
                                              cycle = cycle_dict[track])
                route_dict[track] = current_route

        # Setup of multiple subplots
        number_of_plots = len(route_dict.keys())
        number_of_rows = 2
        number_of_columns = math.ceil(number_of_plots / number_of_rows)
        fig, axs = plt.subplots(number_of_rows, number_of_columns, 
                                sharey = True, figsize=(8, 6))
        axs = axs.reshape(-1)
        # list all route keys
        keys = list(route_dict.keys())
        # Setup of categories along x 
        x_categories = ['From Resistance', 'From Measurement']
        # 
        for element in range(number_of_plots):
                # Get ID of current track
                current_track_id = route_dict[keys[element]].cycle.cycle_id
                # Get energy value from measurement via evaluation functio
                total_measured_energy, recuperation_energy =  calculate_measured_energy(validation_cycle_dict[current_track_id])
                # Create a vector containing the total energy coming out of the traction battery
                y_energies_no_recu = [route_dict[keys[element]].positive_energy, total_measured_energy 
                                      + (-1 * recuperation_energy)]
                # Create a vector containing the total consumed energy, with respect to recuperation
                y_energies_with_recu = [route_dict[keys[element]].resulting_energy, total_measured_energy]
                # Calculate relative track energy error no recuperation
                abs_error_no_recu = (y_energies_no_recu[1] - y_energies_no_recu[0])
                # Calculate relative track energy error with recuperation
                abs_error_with_recu = (y_energies_with_recu[1] - y_energies_with_recu[0])
                # Energy bar without recuperation
                axs[element].bar(x_categories, y_energies_no_recu, edgecolor = '#000000',
                                 color = '#57AB27', label = 'Recuperation Energy')
                # Energy bar with recuperation
                axs[element].bar(x_categories, y_energies_with_recu, 
                                 color = '#00549F', label = 'Total consumed Energy', edgecolor = '#000000')
                # Subplot title containing error
                axs[element].set_title('Absolute Error of Total consumed Energy \n' + current_track_id 
                                       + ' = ' + str(np.floor(abs_error_with_recu * 1000)/1000) + 'kWh',  
                                       fontname = 'Arial', fontsize = 12)
                # Setting subplot ylabels
                axs[element].set_ylabel('Energy Consumption / kWh', fontname = 'Arial')
                
                # Add value labels to the bars
                for i, (value, category) in enumerate(zip(y_energies_with_recu, x_categories)):
                        axs[element].text(i, value, str(round(value, 2)),
                        ha='center', va='bottom')
                
        # Create a legend in empty subplot spot
        legend_ax = fig.add_subplot(2, 2, 4)
        # Turn off axis for the legend subplot
        legend_ax.axis('off')  
        # Create the actual legend
        legend_ax.legend(handles = [plt.Rectangle((0, 0), 1, 1, fc='#57AB27'), 
                                    plt.Rectangle((0, 0), 1, 1, fc='#00549F')],
                         labels = ['Recuperation Energy', 'Total consumed Energy'],
                         loc = 'center', prop={'family' : 'Arial'}) 
        
        # Remove empty subplots if the number of keys is less than the calculated number of subplots
        for element in range(number_of_plots, number_of_rows * number_of_columns):
                fig.delaxes(axs[element])
        # 
        fig.suptitle('Comparison of energy consumption between measurement and calculation \n' 
                     + 'Varied parameter: ' + varying_parameter, fontname = 'Arial', 
                     fontsize = 14 , weight = 'bold')
        # Adjust the spacing between subplots
        plt.tight_layout()
        # 
        if saveplot == True: 
                plt.savefig('Validation Plots ' + varying_parameter + '.svg')
        elif saveplot == False: 
                plt.show()

# create_plotting_data
def create_plotting_data(varying_parameter, route_dict):
        '''
        Create plotting data for the specified varying parameter and route dictionary.

        Args:
                varying_parameter (str): The parameter to vary Must be one of 'Speed',
                        'Distance', or 'Altitude'.
                route_dict (dict): The dictionary containing route data.

        Returns:
                tuple: A tuple containing the following elements:
                - x_data (ndarray): Array of x-axis data based on the varying parameter.
                - y_data (ndarray): Array of y-axis data representing energy consumption.
                - track_list (Series): Series containing track names.
                - vehicle_id (int): The vehicle ID associated with the route.

        Raises:
                TypeError: If the varying_parameter is not a string or the route_dict is not a dictionary.
                ValueError: If the varying_parameter is not one of 'Speed', 'Distance', or 'Altitude'.

        '''
        # Raising errors for the parameter string
        if not isinstance(varying_parameter, str):
                raise TypeError('Please enter the varied parameter as a string.')
        if varying_parameter not in ['Speed', 'Distance', 'Altitude']:
                raise ValueError('Please enter a valid Parameter ("Altitude", "Distance" or "Speed"')

        # Raising errors for route dictionary
        if not isinstance(route_dict, dict):
                raise TypeError('Please enter the route data as a dictionary.')
        
        # Creating vectors to be plotted
        keys = list(route_dict.keys())
        plotting_data = pd.DataFrame()
        # 
        for key in keys: 
                # create a new row for each evaluated route
                current_row = pd.DataFrame([
                        {
                                'Track' : route_dict[key].cycle.cycle_id, 
                                'Total Distance [m]' : route_dict[key].cycle.total_distance, 
                                'Total Mean Speed [km/h]' : route_dict[key].cycle.total_mean_speed, 
                                'Climbed Altitude [hm]' : route_dict[key].cycle.climbed_altitude,
                                'Energy Consumption [kWh]' :  route_dict[key].resulting_energy 
                        }
                ])
                # Append notable data to existing dataframe
                plotting_data = pd.concat([plotting_data, current_row])

        # find the matching column based on the input parameter
        matching_strings = [string for string in list(plotting_data.columns) 
                            if varying_parameter.lower() in string.lower()]
        # Get the parameter value depending on the previously specified parameter
        x_data = np.array(plotting_data[matching_strings[0]])
        # Get the energy consumption for each parameter
        y_data = np.array(plotting_data['Energy Consumption [kWh]'])
        # Get every track name as description
        track_list = plotting_data['Track']
        # 
        vehicle_id = route_dict[key].vehicle.vehicle_id 
        # 
        return x_data, y_data, track_list, vehicle_id 

def plot_parameter_varation(regression_object, x_data, y_data, track_list, 
                            vehicle_id, varying_parameter, save_plot):
        '''
        Plot the datapoints and the resulting linear regression of the specified paramter against energy

        Args:
                regression_object: The regression object containing predicted values.
                x_data (ndarray): Array of x-axis data.
                y_data (ndarray): Array of y-axis data representing energy consumption.
                track_list (Series): Series containing track names.
                vehicle_id (str): The ID of the vehicle.
                varying_parameter (str): The parameter that was varied.
                save_plot (bool): Whether to save the plot or not.

        Returns:
                None

        Raises:
                TypeError: If regression_object is not provided or is of an invalid type.
                ValueError: If varying_parameter is not of type string.
        '''
        # Check if regression_object is provided and of the correct type
        if regression_object is None or not isinstance(regression_object, classes.LinearRegressionObject):
                raise TypeError('Please provide a valid regression object.')

        # Check if varying_parameter is provided and not an empty string
        if not isinstance(varying_parameter, str):
                raise ValueError('Please provide varying parameter as a string.')
        
        # Call linear regression function
        y_predict = regression_object.y_predict
        
        n_data = []
        for element in track_list: 
                current_element = element.split('_')
                n_data.append(current_element[0] + ' ' + current_element[1])

        # Plotting
        fig, ax = plt.subplots()
        ax.grid()
        ax.scatter(x = x_data, y = y_data, c = '#7A6FAC', edgecolors = '#000000')
        ax.plot(x_data, y_predict, color = '#00549F')


        # Parameter dependent xlabels
        if varying_parameter == 'Altitude': 
                ax.set_xlabel('Total Altitude climbed / Hm')
        elif varying_parameter == 'Distance': 
                ax.set_xlabel('Total Distance driven / m')
        elif varying_parameter == 'Speed': 
                ax.set_xlabel('Average nonzero speed / (km/h)')

        # Energy on y-axis
        ax.set_ylabel('Energy Consumption / kWh')
        fig.suptitle(vehicle_id + ' Linear Regression for ' + varying_parameter)

        # Annotate each scatter point with the respective text from 'n'
        for i, txt in enumerate(n_data):
                ax.annotate(txt, (x_data[i], y_data[i]), xycoords='data', 
                            xytext=(3, 3), textcoords='offset points')

                
        # If plot is to be saved, do not show 
        if save_plot == True: 
                plt.savefig('Regression Plots ' 
                            + varying_parameter + '.svg')
        else: 
        # Show plot only if it is not being saved‚
                plt.show()

        return

def parameter_sensitivity_analysis(parameter_list, cycle_df, plot_regression, vehicle_parameters, cycle_dict, save_plot): 
        '''
        Perform parameter sensitivity analysis and calculate regression coefficients.

        Args:
                parameter_list (list): A list of tuples containing parameter information. Each tuple
                should have the following format: (varying_parameter, deviation).
                cycle_df (DataFrame): The cycle DataFrame.
                cycle_dict (dict): A dictionary containing the time dependent cycle data.
                plot_regression (bool): Whether to plot the regression or not.
                vehicle_parameters: The parameters for the vehicle model.
                save_plot (bool): Whether to save the plot or not.

        Returns:
                tuple: A tuple containing the following elements:
                - std_betas (list): A list of standardized regression coefficients.
                - x_categories (list): A list of x-axis categories.

        Raises:
                ValueError: If the parameter_list is empty.
                ValueError: If cycle_df is not of type pandas.Dataframe.
        '''

        # Check if parameter_list is empty
        if not parameter_list:
                raise ValueError('Please provide a valid parameter list.')
        # Assure cycle_df is a dataframe
        if not isinstance(cycle_df, pd.DataFrame):
                raise TypeError('The cycle_df parameter must be entered as a pandas.DataFrame.')
        
        # Create lists to save standardized betas and x-categories in
        std_betas = []
        x_categories = [] 

        for input in parameter_list: 
                # Get important data from tuple
                varying_parameter = input[0]
                deviation = input[1]
                # Get the all tracks to be evaluated per parameter
                parameter_cycles, parameter_cycles_list = find_parameter_cycles(
                                                                cycle_df = cycle_df, 
                                                                keyword = varying_parameter, 
                                                                deviation = deviation
                                                        )
                # Evaluate the chosen routes
                route_dict = evaluate_routes(cycles = parameter_cycles_list, cycle_dict = cycle_dict,
                                             vehicle_model = vehicle_parameters)
                # Receive x- and y-data to calculate linear regression for each parameter
                x_data, y_data, track_list, vehicle_id = create_plotting_data(varying_parameter = varying_parameter, 
                                                                              route_dict = route_dict)
                
                # Calculate unstandardized and standardized regression coefficients
                regression_object = classes.LinearRegressionObject(x_data = x_data, y_data = y_data)

                if plot_regression == True: 
                        # If plot is to be saved, no plot will be shown
                        plot_parameter_varation(
                                varying_parameter = varying_parameter, 
                                regression_object = regression_object, 
                                track_list = track_list, 
                                vehicle_id = vehicle_id, 
                                save_plot = save_plot,
                                y_data = y_data, 
                                x_data = x_data
                        )
                # Regression results
                std_betas.append(regression_object.std_coeff)
                x_categories.append(varying_parameter)

        return std_betas, x_categories

def multiple_linear_regression(cycle_df, cycle_dict, vehicle_parameters, plot_mlr_data):
        '''
        Perform multiple linear regression on given cycle and vehicle data.

        Args:
                cycle_df (DataFrame): The cycle DataFrame.
                cycle_dict (dict): The cycle dictionary.
                vehicle_parameters (classes.VehicleParameter): The parameters for the vehicle model.
                plot_mlr_data (bool): Bool to decide wether to plot mlr data or not

        Returns:
                tuple: A tuple containing the following elements:
                - mlr_std_coeff (ndarray): An array of standardized regression coefficients.
                - x_categories (list): A list of x-axis categories.
                - scores (ndarray): An array of cross-validation scores.

        Raises:
                ValueError: If cycle_df is not a pandas Dataframe.
                ValueError: If cycle_dict is not a dictionary.
                ValueError: If vehicle_parameters is not a VehicleParameters object.
        '''

        # Check if cycle_df is a pandas Dataframe
        if not isinstance(cycle_df, pd.DataFrame):
                raise ValueError('Please provide cycle parameter info as a pandas Dataframe.')
        # Check if cycle_dict is a dictionary
        if not isinstance(cycle_dict, dict):
                raise ValueError('Please provide cycle dictionary as a dictionary.')
        # Check if vehicle_parameters is a VehicleParameters object
        if not isinstance(vehicle_parameters, classes.VehicleParameters):
                raise ValueError('Please provide vehicle data as a VehicleParameters object.')
        
        # Evaluate all routes to create largest possible dataset
        mlr_route_dict = evaluate_routes(cycles = list(cycle_df['ID']), cycle_dict = cycle_dict,
                                         vehicle_model = vehicle_parameters)
        # Create y-data by iterating through 
        y_data = []
        for route in mlr_route_dict.keys(): 
                y_data.append(mlr_route_dict[route].resulting_energy)  
        
        # Concat vectors to create three dimensional numpy array as x-inputs
        x_data = pd.concat([cycle_df['Climbed Altitude [hm]'], cycle_df['Total Distance [m]'],  
                        cycle_df['Total Mean Speed [km/h]']], axis = 1)
        # 
        x_categories = ['Altitude', 'Distance', 'Speed']
        
        if plot_mlr_data == True: 
                # energy_values = pd.DataFrame(y_data)
                plot_parallel_coords(cycle_df = cycle_df, energy_values = y_data)

        # Create numpy array from pandas
        x_data = x_data.values
        
        # Split Data into training and testing datasets
        x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, 
                                                            test_size = 0.4,
                                                            shuffle = True, 
                                                            random_state=0)
        # Standardize x-values for training 
        scaler = StandardScaler()
        x_train_transformed = scaler.fit_transform(x_train)

        # Train model on training data set
        multiple_regression = LinearRegression().fit(x_train_transformed, y_train)
        
        # Standardize x-Values for validation
        x_test_transformed = scaler.fit_transform(x_test)

        # Perform cross-validation and obtain the scores
        mlr_cv_score = cross_val_score(multiple_regression, x_train_transformed, 
                                       y_train, cv=5, scoring = 'r2')

                
        mlr_std_coeff = multiple_regression.coef_ / np.std(x_train_transformed, axis=0)

        test_dataset_score = multiple_regression.score(x_test_transformed, y_test)

        return mlr_std_coeff, x_categories, mlr_cv_score, test_dataset_score

def plot_std_betas(std_betas, x_categories, type, saveplot):
        '''
        Plot the standardized coefficients as a bar plot.

        Args:
                std_betas (list): A list of standardized coefficients.
                x_categories (list): A list of x-axis categories.
                type (str): The type of coefficients. Options are 'psa' for Parameter Sensitivity Analysis
                coefficients or 'mlr' for multiple linear regression coefficients.
                saveplot (bool): If true, the plot will be saved but not shown, if false, the other way around.

        Returns:
                None

        Raises:
                ValueError: If the type is not 'psa' or 'mlr'.
        '''
        if type == 'psa': 
                title_str = 'Sensitvity Analysis'
        elif type == 'mlr': 
                title_str = 'Multiple Linear Regression'
        else:
                raise ValueError('Please enter "psa" for Parameter Sensitvity Analysis coefficients'
                                 'or "mlr" for multiple linear regression coefficients.')
        
        # Plot all coefficients inside a bar plot
        fig, ax = plt.subplots()
        # assign colors based on positive or negative values 
        colors = ['#00549F', '#7A6FAC', '#612158']
        # Plot bars
        ax.bar(x_categories, std_betas, color = colors, edgecolor = '#000000')
        # Set y-label
        ax.set_ylabel('Standardized Coefficients - Absolute Values', 
                      fontname = 'Arial', fontsize = 12)
        fig.suptitle('Standardized Coefficients Comparison - ' + title_str, 
                     fontname = 'Arial', fontsize = 12, weight = 'bold')

        # Add value labels to the bars
        for i, (value, category) in enumerate(zip(std_betas, x_categories)):
                label_y_pos = value if value >= 0 else value - 0.04  # Adjust y-position for negative values
                ax.text(i, label_y_pos, str(round(value, 2)), ha='center', va='bottom')
        
        # Check if plot is being saved 
        if saveplot == True:
                plt.savefig('Beta Bars ' + title_str + '.svg')
        elif saveplot == False: 
                plt.show()

def plot_coefficients_sankey(std_betas, x_categories):
        '''
        Plot a Sankey diagram to visualize the influence of each coefficient value.

        Args:
                std_betas (list): A list of standardized beta coefficients.
                x_categories (list): A list of category labels for x-axis.

        Returns:
                None
        
        Raises:
                TypeError: If beta values are not provided as a list.
                TypeError: If category strings are not provided as a list.
                ValueError: If there are more than three beta values.
                ValueError: If the list of categories and coefficients are not of the same size.

        '''

        # Check datatypes
        if not isinstance(std_betas, list): 
                raise TypeError('Make sure beta values are provided as a list.')
        if not isinstance(x_categories, list): 
                raise TypeError('Make sure cataegory strings are provided as a list.')
        if len(std_betas) > 3: 
                raise ValueError('There can only be a maximum of three beta values.')
        if not len(std_betas) == len(x_categories): 
                raise ValueError('List of categories and list of coefficients must be of the same size.')
        
        # Create node labels for sankey diagram
        node_label = [x_categories[0], x_categories[1], x_categories[2], 'Energy Consumption']
        # Calculate graph weights in percentage
        beta_total = sum(map(abs, std_betas))
        # set weights for sankey
        values = [std_betas[0]/beta_total * 100,
                  std_betas[1]/beta_total * 100, 
                  std_betas[2]/beta_total * 100]
        # Convert labels into values, sankey doesn't recognise strings
        source_node = [0, 1, 2]
        target_node = [3, 3, 3]
        # 
        node_color = ['#00549F', '#7A6FAC', '#612158', ]
        # 
        fig = go.Figure( 
                data=[go.Sankey( 
                        node = dict( 
                        label = node_label,
                        color = node_color
                        ),
                        # Linking nodes
                        link = dict(
                        source = source_node,
                        target = target_node,
                        value = values
                        ))])

        # With this save the plots 
        plot(fig, 
             image_filename='sankey_diag_psa', 
             image='svg', 
        )
        # And shows the plot
        fig.show()
        return

def run_psa_cycle_validation(cycle_df, cycle_dict, parameter_list, vehicle_parameters, filepath, saveplot):
        '''
        Plot comparison of energy consumption plots for all parameters 
        that go through parameter sensitvity analysis.

        Args:
                cycle_df (pandas.DataFrame): DataFrame containing cycle data.
                cycle_dict (dict): dictionary of all cycles and their respective properties.
                parameter_list (list): List of tuples of parameter and deviation for validation.
                filepath (str): String containing the folder path
                saveplot (bool): Bool checking wether the plots should be saved or not
        Returns:
                None

        Raises: 
                TypeError: If cycle_df is not a pandas DataFrame.
                TypeError: If parameter_list is not a list.
        '''

        # Check data types
        if not isinstance(cycle_df, pd.DataFrame):
                raise TypeError('cycle_df must be a pandas DataFrame.')
        if not isinstance(cycle_dict, dict):
                raise TypeError('cycle_df must be a pandas DataFrame.')
        if not isinstance(parameter_list, list):
                raise TypeError('parameter_list must be a list.')
        
        # Get all cycles that can be used for validation
        cycles_for_validation = list_validation_cycles(filepath)
        # 
        for parameter in parameter_list: 
                # Find number of cycles to chose from
                parameter_df, parameter_track_list = find_parameter_cycles(cycle_df = cycle_df, keyword = parameter[0], 
                                                                           deviation = parameter[1])
                # Find intersection between chosen cycles and cycles open for validation
                viable_tracks = intersect_cycles(parameter_tracks = parameter_track_list, 
                                                 validation_tracks = cycles_for_validation)
                # 
                validation_plots(
                        cycle_dict = cycle_dict,
                        vehicle_parameters = vehicle_parameters,
                        viable_track_list = viable_tracks,
                        cycles_for_validation = cycles_for_validation,
                        saveplot = saveplot,
                        varying_parameter = parameter[0]
                )

def plot_parallel_coords(cycle_df, energy_values): 
        '''
        Plot parallel coordinates of all datasets.

        Args:
                cycle_df (pandas.DataFrame): DataFrame containing cycle data.
                energy_values (pandas.DataFrame): DataFrame containing calculated energy values.

        Returns:
                None

        Raises: 
                TypeError: If parameter_list is not a list.
        '''

        # Check attribute datatypes
        if not isinstance(cycle_df, pd.DataFrame):
                raise TypeError('cycle_df must be a pandas DataFrame.')
        
        y_data = energy_values
        # Concatenate cycle data into track properties
        x_data_df = pd.concat([cycle_df['ID'], cycle_df['Climbed Altitude [hm]'], cycle_df['Total Distance [m]'],  
                        cycle_df['Total Mean Speed [km/h]']], axis = 1)
        x_data_df.reset_index(drop=True, inplace=True)

        # Turn energy_data into pandas dataframe
        y_data_df = pd.DataFrame(y_data, columns = ['Energy Consumption [kWh]'])

        # Get Ranks from track numbers
        rank_numbers = [int(s.split('_')[1]) for s in list(cycle_df['ID'])]
        rank_df = pd.DataFrame(rank_numbers, columns=['Rank'])
        
        # Concatenate all data into a full input dataframe 
        parallel_coordinates_data = pd.concat([rank_df, x_data_df, y_data_df], axis=1)

        # Define dimensions for parallel coordinates plotting
        dimensions = list([
                dict(range=(parallel_coordinates_data['Rank'].min(), parallel_coordinates_data['Rank'].max()), tickvals = parallel_coordinates_data['Rank'], 
                            ticktext = parallel_coordinates_data['ID'],label='Track', values = parallel_coordinates_data['Rank']),
                dict(range=(parallel_coordinates_data['Climbed Altitude [hm]'].min(),parallel_coordinates_data['Climbed Altitude [hm]'].max()),
                                label='Climbed Altitude [hm]', values=parallel_coordinates_data['Climbed Altitude [hm]']),
                dict(range=(parallel_coordinates_data['Total Distance [m]'].min(),parallel_coordinates_data['Total Distance [m]'].max()),
                                label='Total Distance [m]', values=parallel_coordinates_data['Total Distance [m]']),
                dict(range=(parallel_coordinates_data['Energy Consumption [kWh]'].min(),parallel_coordinates_data['Energy Consumption [kWh]'].max()),
                                label='Energy Consumption [kWh]', values=parallel_coordinates_data['Energy Consumption [kWh]']),
                dict(range=(parallel_coordinates_data['Total Mean Speed [km/h]'].min(),parallel_coordinates_data['Total Mean Speed [km/h]'].max()),
                                label='Total Mean Speed [km/h]', values=parallel_coordinates_data['Total Mean Speed [km/h]'])
        ])
        
        # plot parallel coordinates plot 
        fig = go.Figure(data= go.Parcoords(line = dict(color = parallel_coordinates_data['Rank'], colorscale = 'agsunset'), dimensions = dimensions))
        fig.show()
