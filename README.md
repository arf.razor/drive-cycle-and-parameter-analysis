# Dcnpanalysis

Dcnpanalysis is a Python package designed to examine the correlation between drive cycle parameters and the energy consumption of a battery electric vehicle. The code is specifically designed to evaluate an open source dataset containing drive cycles measured in a VW ID.3, collected by the TU Munich. The referenced dataset can be found [here](https://mediatum.ub.tum.de/1656313) as well as general information about the study in the frame of which this data was collected. 

## Installation

The package is installable via pip and the git link: 

```bash
pip install git+https://git.rwth-aachen.de/arf.razor/drive-cycle-and-parameter-analysis.git
```

## Usage

In order to make full use of the package it is necessary to first download the data referenced above. 
To make the package work in your python environment, import the functions and classes module. 
A more detailed description and examples regarding the functionality can be found inside the Jupyter file [demo.ipynb](https://git.rwth-aachen.de/arf.razor/drive-cycle-and-parameter-analysis/-/blob/main/example/demo.ipynb) in the examples folder. 
Please note that the Jupyter python extension is not a dependency of the package and will not be installed during installation of the packe. 

```python
import dcnpanalysis.classes
import dcnpanalysis.functions 
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

This package is lice send under the terms of the [MIT](https://choosealicense.com/licenses/mit/)license.

