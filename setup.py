import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name = 'dcnpanalysis',
    version = '0.0.1',
    author = 'Steffen Thevis',
    author_email = 'steffen.thevis@rwth-aachen.de',
    description = 'Tools to evaluate parameters influence on electric vehicle drive cycle consumption',
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = 'https://git.rwth-aachen.de/arf.razor/drive-cycle-and-parameter-analysis',
    license='MIT',
    packages = ['dcnpanalysis'],
    py_modules = ['dcnpanalysis.classes', 'dcnpanalysis.functions'],
    install_requires=[
                        'pandas', 
                        'jinja2', 
                        'ipython',
                        'numpy',
                        'pysimplegui',
                        'matplotlib',
                        'scikit-learn',
                        'plotly',
                        'pip',
                      ]
)